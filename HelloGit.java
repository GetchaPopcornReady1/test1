/*
 *File name: HelloGit.java
 *
 *Programmer: Josh Vetter
 * ULID: jvetter
 *
 *Date: Aug 24, 2016
 *
 *Class: IT 179
 *
 *Lecture Section: 001
 *Lecture Instructor: Holbrook
 */


public class HelloGit
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		System.out.println("hello git!");

	}

}
